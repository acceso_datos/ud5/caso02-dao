package vista;

import java.util.List;

import dao.ClienteDAO;
import dao.ClienteDAOFile;
import excepciones.ConnectionException;
import excepciones.DataAccessException;
import modelo.Cliente;

public class App {

	public static void main(String[] args) {

		try {
			ClienteDAO clidao = new ClienteDAO();
//			ClienteDAOFile clidao = new ClienteDAOFile();
			
			// Ejercicio. Añadir método size en el DAO.
//			System.out.println("Num registros: " + clidao.size());

			List<Cliente> clientes = clidao.findAll();
			for (Cliente cli : clientes) {
				System.out.println(cli);
			}
			Cliente cli = clidao.find(1);
			if (cli != null) {
				System.out.println("Encontrado cli 1: " + cli);
			}

			// insertar
			Cliente cliInsertar = new Cliente("sergiodao11", "direccion dao11");
			cliInsertar = clidao.insert(cliInsertar);
			if (cliInsertar != null) {
				System.out.println("Insertado!");
				System.out.println("\t" + cliInsertar);
				clientes.add(cliInsertar);
			}

			// actualizar
			Cliente cliModificar = clidao.find(2);
			if (cliModificar != null) {
				cliModificar.setNombre("otro nombre");
				cliModificar.setDireccion("otra direccion");
				if (clidao.update(cliModificar)) {
					System.out.println("Cliente modificado!!");
				}
			}

			// eliminar con error bd
			if (clidao.delete(1)) {
				System.out.println("Cliente borrado!!");
			}
			
			// eliminar ok
			Cliente cliEliminar = clidao.find(7);
			if (cliEliminar != null) {
				if (clidao.delete(cliEliminar)) {
					System.out.println("Cliente borrado!!");
				}
			}

			clidao.cerrar();

		} catch (ConnectionException | DataAccessException e) {
			System.out.println("Error: " + e.getMessage());
		}

	}

}
