package dao;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

import excepciones.DataAccessException;
import modelo.Cliente;

public class ClienteDAOFile implements GenericDAO<Cliente> {

	private final RandomAccessFile raf;
	static final short TAM = 284; // total bytes que ocupa el registro en memoria: 4+120+160
	static final short TNOMBRE = 60; // tamaño máximo campo nombre: 2 bytes por caracter
	static final short TDIRECCION = 80; // tamaño máximo campo apellidos: 2 bytes por caracter

	public ClienteDAOFile() {
		raf = ConexionFile.getConexion();
	}

	public Cliente find(int id) throws DataAccessException {
		Cliente cli;
		try {
			cli = buscar(id);
			return cli;
		} catch (IOException e) {
			throw new DataAccessException("Error buscando cliente por id: " + e.getMessage());
		}
	}

	public List<Cliente> findAll() throws DataAccessException {
		List<Cliente> lista = new ArrayList<Cliente>();
		try {
			raf.seek(0);
			while (raf.getFilePointer() < raf.length()) {
				Cliente cli = leer();
				if (cli.getId() > 0) {
					lista.add(cli);
				}
			}
			return lista;
		} catch (IOException e) {
			throw new DataAccessException("Error obteniendo todos los clientes: " + e.getMessage());
		}

	}

	public Cliente insert(Cliente cliNuevo) throws DataAccessException {
		int idAutonum = 1;
		try {
			if (raf.length() > 0) {
				raf.seek(raf.length() - TAM);
				idAutonum = Math.abs(raf.readInt());
				idAutonum++;
			}
			cliNuevo.setId(idAutonum);
			raf.seek(raf.length());
			escribir(cliNuevo);
			return cliNuevo;
		} catch (IOException e) {
			throw new DataAccessException("Error al intentar insertar cliente: " + e.getMessage());
		}

	}

	public boolean update(Cliente cliModif) throws DataAccessException {

		try {
			Cliente cli = buscar(cliModif.getId());
			if (cli != null) {
				escribir(cliModif);
				return true;
			}
			return false;
		} catch (IOException e) {
			throw new DataAccessException("Error al intentar modificar cliente: " + e.getMessage());
		}

	}

	public boolean delete(int id) throws DataAccessException {
		Cliente cli;
		try {
			cli = buscar(id);
			if (cli != null) {
				cli.setId(cli.getId() * (-1));
				escribir(cli);
				return true;
			}
			return false;
		} catch (IOException e) {
			throw new DataAccessException("Error al intentar eliminar cliente: " + e.getMessage());
		}

	}

	public boolean delete(Cliente t) throws DataAccessException {
		return this.delete(t.getId());
	}

	private Cliente leer() throws IOException {
		int id; // 2 bytes. Si es negativo indica registro borrado
		StringBuilder nombre; // max. 20 caracteres -> 40 bytes (2 bytes por caracter)
		StringBuilder direccion; // max. 40 caracteres -> 80 bytes
		id = raf.readInt();
		String aux = "";
		for (int i = 0; i < TNOMBRE; i++) {
			aux = aux + raf.readChar();
		}
		nombre = new StringBuilder(aux);
		nombre.setLength(TNOMBRE);
		aux = "";
		for (int i = 0; i < TDIRECCION; i++) {
			aux = aux + raf.readChar();
		}
		direccion = new StringBuilder(aux);
		direccion.setLength(TDIRECCION);
		return new Cliente(id, nombre.toString(), direccion.toString());
	}

	private void escribir(Cliente c) throws IOException {
		StringBuilder nombre = new StringBuilder(c.getNombre());
		nombre.setLength(TNOMBRE);
		StringBuilder direccion = new StringBuilder(c.getDireccion());
		direccion.setLength(TDIRECCION);
		raf.writeInt(c.getId());
		raf.writeChars(nombre.toString());
		raf.writeChars(direccion.toString());
	}

	private Cliente buscar(int id) throws IOException {
		Cliente cli = null;
		boolean encontrado = false;
		raf.seek(0);
		while (!encontrado && raf.getFilePointer() < raf.length()) {
			cli = leer();
			if (cli.getId() == id) {
				encontrado = true;
			}
		}
		if (!encontrado) {
			return null;
		} else {
			raf.seek(raf.getFilePointer() - TAM);
			return cli;
		}
	}

	public void cerrar() {
		ConexionFile.cerrar();
	}
	

}
