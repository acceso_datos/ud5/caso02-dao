package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import excepciones.DataAccessException;
import modelo.Cliente;


public class ClienteDAO implements GenericDAO<Cliente> {
	final static String TABLA = "cliente";
	final static String PK = "id";
	
	final String SQLSELECTALL = "SELECT * FROM " + TABLA;
	final String SQLSELECTPK = "SELECT * FROM " + TABLA + " WHERE " + PK + " = ?";
	final String SQLINSERT = "INSERT INTO " + TABLA + " (nombre, direccion) VALUES (?, ?)";
	final String SQLUPDATE = "UPDATE " + TABLA + " SET nombre = ?, direccion = ? WHERE " + PK + " = ?";
	final String SQLDELETE = "DELETE FROM " + TABLA + " WHERE " + PK + " = ?";
	private final PreparedStatement pstSelectPK;
	private final PreparedStatement pstSelectAll;
	private final PreparedStatement pstInsert;
	private final PreparedStatement pstUpdate;
	private final PreparedStatement pstDelete;

	public ClienteDAO() throws DataAccessException {
		Connection con = ConexionBD.getConexion();
		try {
			pstSelectPK = con.prepareStatement(SQLSELECTPK);
			pstSelectAll = con.prepareStatement(SQLSELECTALL);
			pstInsert = con.prepareStatement(SQLINSERT, PreparedStatement.RETURN_GENERATED_KEYS);
			pstUpdate = con.prepareStatement(SQLUPDATE);
			pstDelete = con.prepareStatement(SQLDELETE);
		} catch (SQLException e) {
			throw new DataAccessException("Error preparando capa de acceso a datos: " + e.getMessage());
		}
		
	}

	public void cerrar() throws DataAccessException {
		try {
			pstSelectPK.close();
			pstSelectAll.close();
			pstInsert.close();
			pstUpdate.close();
			pstDelete.close();
		} catch (SQLException e) {
			throw new DataAccessException("Error cerrando capa de acceso a datos: " + e.getMessage());
		}
		
	}

	private Cliente build(int id, String nombre, String direccion) {
		return new Cliente(id, nombre, direccion);
	}

	public Cliente find(int id) throws DataAccessException {
		Cliente c = null;
		try {
			pstSelectPK.setInt(1, id);
			ResultSet rs = pstSelectPK.executeQuery();
			if (rs.next()) {
				c = build(id, rs.getString("nombre"), rs.getString("direccion"));
			}
			rs.close();
			return c;
		} catch (SQLException e) {
			throw new DataAccessException("Error buscando cliente por id: " + e.getMessage());
		}
	}

	public List<Cliente> findAll() throws DataAccessException {
		List<Cliente> listaClientes = new ArrayList<Cliente>();
		try {
			ResultSet rs;
			rs = pstSelectAll.executeQuery();
			while (rs.next()) {
				listaClientes.add(build(rs.getInt("id"), rs.getString("nombre"), rs.getString("direccion")));
			}
			rs.close();
			return listaClientes;
		} catch (SQLException e) {
			throw new DataAccessException("Error obteniendo todos los clientes: " + e.getMessage());
		}

	}

	public Cliente insert(Cliente cliInsertar) throws DataAccessException {
		try {
			pstInsert.setString(1, cliInsertar.getNombre());
			pstInsert.setString(2, cliInsertar.getDireccion());
			int insertados = pstInsert.executeUpdate();
			if (insertados == 1) {
				ResultSet rsClave = pstInsert.getGeneratedKeys();
				rsClave.next();
				int idAsignada = rsClave.getInt(1);
				cliInsertar.setId(idAsignada);
				rsClave.close();
				return cliInsertar;
			}
			return null;
		} catch (SQLException e) {
			throw new DataAccessException("Error al intentar insertar cliente: " + e.getMessage());
		}

	}

	public boolean update(Cliente cliActualizar) throws DataAccessException {
		try {
			pstUpdate.setString(1, cliActualizar.getNombre());
			pstUpdate.setString(2, cliActualizar.getDireccion());
			pstUpdate.setInt(3, cliActualizar.getId());
			int actualizados = pstUpdate.executeUpdate();
			return (actualizados == 1);
		} catch (SQLException e) {
			throw new DataAccessException("Error al intentar modificar cliente: " + e.getMessage());
		}

	}

	public boolean delete(int id) throws DataAccessException {
		try {
			pstDelete.setInt(1, id);
			int borrados = pstDelete.executeUpdate();
			return (borrados == 1);
		} catch (SQLException e) {
			throw new DataAccessException("Error al intentar eliminar cliente: " + e.getMessage());
		}

	}

	public boolean delete(Cliente cliEliminar) throws DataAccessException {
		return this.delete(cliEliminar.getId());
	}

}
