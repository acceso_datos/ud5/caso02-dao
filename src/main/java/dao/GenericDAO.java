package dao;

import java.util.List;

import excepciones.DataAccessException;

public interface GenericDAO<T> {
	T find(int id) throws DataAccessException;
    
    List<T> findAll() throws DataAccessException; 
    
    T insert(T t) throws DataAccessException;
     
    boolean update(T t) throws DataAccessException;
     
    boolean delete(int id) throws DataAccessException;
    boolean delete(T t) throws DataAccessException;    
}
